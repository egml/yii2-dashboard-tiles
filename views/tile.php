<a href="<?= $url ?>" class="thumbnail egml-dashboard_tiles-tile<?= $inProgress ? ' egml-dashboard_tile--in_progress' : '' ?>"<?= $inProgress ? ' onclick="alert(\'В разработке\'); return false;"':'' ?>>
	<span class="egml-dashboard_tiles-tile-screen">
		<span class="egml-dashboard_tiles-tile-icon glyphicon glyphicon-<?= $icon ?><?= $additionalIconClass ?>"></span>
	</span>
	<span class="egml-dashboard_tiles-tile-title"><?= $title ?></span>
	<span class="egml-dashboard_tiles-tile-description">
		<?= $description ?>
	</span>
</a>
