<?php
namespace egml\dashboardTiles;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;

class Widget extends \yii\base\Widget
{
	public $items = [];

	private $_itemDefaults = [
		'url' => '',
		'icon' => 'cog',
		'additionalIconClass' => '',
		'title' => 'Заголовок',
		'description' => '',
		'inProgress' => false,
	];

	public function init()
	{
		// if ($this->cssNamespace === '' && isset(\Yii::$app->params['namespace']['css'])) {
		// 	$this->cssNamespace = \Yii::$app->params['namespace']['css'];
		// }
	}

	public function run()
	{
		Asset::register($this->view);
		$items = '';
		$defaultParams = [];
		if (empty($this->items)) {
			$items = 'Нет данных для отображения';
		} else {
			foreach ($this->items as $key => $item) {
				$params = ArrayHelper::merge(
					$defaultParams,
					$this->_itemDefaults,
					$item
				);
				$params['url'] = Url::to($params['url']);
				$items .= $this->render('tile', $params);
			}
		}
		return $this->render('container', ArrayHelper::merge(
			$defaultParams,
			['content' => $items]
		));
	}
}
