<?php
namespace egml\dashboardTiles;

class Asset extends \yii\web\AssetBundle
{
	public $sourcePath = '@egml/dashboardTiles/public-assets';
	public $css = [
		'style.css',
	];
	public $depends = [
		'yii\\bootstrap\\BootstrapAsset',
	];
}
