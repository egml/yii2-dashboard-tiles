## Набор плиток для Yii2

Обычно применяется на главной странице панели управления сайтом/приложением.

Применение:
	
	<?php
		echo DashboardTiles::widget(['items' => [
			// Значения по умолчанию
			// [
			//     'url' => '',
			//     'icon' => 'cog',
			//     'title' => 'Заголовок',
			//     'description' => '',
			// ],
			[
				'url' => Url::to(['structure/index']),
				'icon' => 'align-left',
				'title' => 'Структура',
				'description' => 'Управление структурой сайта, создание страниц',
			],
			[
				'url' => '',
				'icon' => 'user',
				'title' => 'Клиенты',
				'description' => 'Управление клиентами компании',
				'inProgress' => true,
			],
			[
				'url' => '',
				// 'icon' => 'folder-close',
				'icon' => 'briefcase',
				'title' => 'Проекты',
				'description' => 'Управление проектами',
				'inProgress' => true,
			],
			[
				'url' => Url::to(['settings/index']),
				'icon' => 'cog',
				'title' => 'Настройки',
				'description' => 'Общие настройки сайта и панели управления',
			],
		]]);
